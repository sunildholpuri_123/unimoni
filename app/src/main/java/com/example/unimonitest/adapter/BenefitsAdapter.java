package com.example.unimonitest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.unimonitest.R;
import com.example.unimonitest.pojo.PlanBenefits;
import com.example.unimonitest.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;


public class BenefitsAdapter extends RecyclerView.Adapter<BenefitsAdapter.ViewHolder> {


    public static final int SAVE_VIEW_TYPE = 1;
    private Context context;
    private List<PlanBenefits> PlanList;

    public BenefitsAdapter(Context context, List<PlanBenefits> PlanList) {
        this.context = context;
        this.PlanList = PlanList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ViewHolder viewHolder;
        switch (viewType) {
            case BenefitsAdapter.SAVE_VIEW_TYPE:
                viewHolder = new ViewHolder(LayoutInflater.from(context).inflate(R.layout.save_row_layout, parent, false), viewType);
                break;
            default:
                viewHolder = new ViewHolder(LayoutInflater.from(context).inflate(R.layout.benefits_row_layout, parent, false), viewType);

        }
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return getViewTypeByPos(position);
    }

    private int getViewTypeByPos(int position) {
        return (PlanList != null ? PlanList.get(position).getViewType() : 0);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int pos) {
        int viewType = getViewTypeByPos(pos);
        PlanBenefits planBenefits = PlanList.get(pos);
        switch (viewType) {
            case SAVE_VIEW_TYPE:
                holder.SaveAmount(planBenefits);
                break;
            default:
                holder.Benefits(planBenefits);
                break;
        }


    }

    private int getIcon(String benefitIconLocal) {
        int icon = 0;
        switch (benefitIconLocal) {
            case Constants.IHO_CHECKUP_ICON:
                icon = R.drawable.health_check_icon;
                break;
            case Constants.IHO_DENTAL_ICON:
                icon = R.drawable.dental_check_icon;
                break;
            case Constants.IHO_DISCOUNT_CARD_ICON:
                icon = R.drawable.discount_card_icon;
                break;
            case Constants.IHO_MOBILE_ICON:
            case Constants.IHO_CONSULATION_ICON:
                icon = R.drawable.tele_consult_con;
                break;
            case Constants.IHO_PHARMACY_ICON:
                icon = R.drawable.medicine_icon;
                break;
            case Constants.IHO_DIET_ICON:
                icon = R.drawable.health_fitness_icon;
                break;
        }
        return icon;
    }

    @Override
    public int getItemCount() {
        return PlanList != null && !PlanList.isEmpty() ? PlanList.size() : 0;
    }

    public void updateList(List<PlanBenefits> data) {
        PlanList = data;
        notifyDataSetChanged();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private TextView primaryText, secondaryText;
        private ImageView imageview;

        private ViewHolder(View itemView, int viewType) {
            super(itemView);
            switch (viewType) {
                case SAVE_VIEW_TYPE:
                    primaryText = itemView.findViewById(R.id.primary_text);
                    imageview = itemView.findViewById(R.id.imageview);
                    break;
                default:
                    primaryText = itemView.findViewById(R.id.primary_text);
                    secondaryText = itemView.findViewById(R.id.secondary_text);
                    imageview = itemView.findViewById(R.id.imageview);
                    break;

            }
        }

        private void Benefits(PlanBenefits planBenefits) {

            primaryText.setText(planBenefits.getBenefitTitle());
            if (planBenefits.getBenefitSubTitle() != null && !planBenefits.getBenefitSubTitle().isEmpty()) {
                String secondary_text = "(" + planBenefits.getBenefitSubTitle() + ")";
                secondaryText.setText(secondary_text);
            }
            int icon = getIcon(planBenefits.getBenefitIconLocal());
            Picasso.get().load(icon)

                    .into(imageview);
        }

        private void SaveAmount(PlanBenefits planBenefits) {
            primaryText.setText(planBenefits.getBenefitTitle());
        }
    }
}
