package com.example.unimonitest.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.unimonitest.R;
import com.example.unimonitest.fragment.PlanFragment;

public class PagerAdapter extends FragmentPagerAdapter {

    private String[] titleList;
    private Context context;

    public PagerAdapter(Context context, FragmentManager fm, String[] titleList) {
        super(fm);
        this.titleList = titleList;
        this.context = context;
    }

    @Override
    public Fragment getItem(int index) {
        PlanFragment planFragment;
        Bundle bundle;
        switch (index) {
            case 0:
                planFragment = new PlanFragment();
                bundle = new Bundle();
                bundle.putInt(context.getString(R.string.member_count), 2);
                planFragment.setArguments(bundle);
                return planFragment;
            default:
                planFragment = new PlanFragment();
                bundle = new Bundle();
                bundle.putInt(context.getString(R.string.member_count), 4);
                planFragment.setArguments(bundle);
                return planFragment;
        }
    }

    @Override
    public int getCount() {
        return titleList.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleList[position];
    }
}
