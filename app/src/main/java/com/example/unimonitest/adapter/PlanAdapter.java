package com.example.unimonitest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.unimonitest.R;
import com.example.unimonitest.pojo.PlanBenefits;
import com.example.unimonitest.pojo.PlanModel;

import java.util.List;


public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.ViewHolder> {


    private Context context;
    private List<PlanModel> planModelList;
    private int memberCount;

    public PlanAdapter(Context context, List<PlanModel> planModelList, int memberCount) {
        this.context = context;
        this.planModelList = planModelList;
        this.memberCount = memberCount;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.member_plan_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int pos) {

        PlanModel planModel = planModelList.get(pos);

        holder.title.setText(planModel.getPlanName());
        String rupees = context.getResources().getString(R.string.rupee_symbol);
        String price = rupees + " " + String.valueOf(planModel.getPlanCost());
        holder.price.setText(price);

        String primaryText = context.getString(R.string.buy_now_for) + " " + String.valueOf(planModel.getPlanCost());
        holder.primaryText.setText(primaryText);

        String secondaryText = "(Up to " + String.valueOf(memberCount) + " family members)";
        holder.secondaryText.setText(secondaryText);
        holder.Benefits(planModel);
    }


    @Override
    public int getItemCount() {
        return planModelList != null && !planModelList.isEmpty() ? planModelList.size() : 0;
    }

    public void updateList(List<PlanModel> data, int memberCount) {
        planModelList = data;
        this.memberCount = memberCount;
        notifyDataSetChanged();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {


        private TextView title, price,
                primaryText, secondaryText;
        private BenefitsAdapter benefitsAdapter;
        private RecyclerView recyclerview;

        private ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            primaryText = itemView.findViewById(R.id.primary_text);
            secondaryText = itemView.findViewById(R.id.secondary_text);
            recyclerview = itemView.findViewById(R.id.recyclerview);
            recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
        }

        private void Benefits(PlanModel planModel) {
            PlanBenefits planBenefits = new PlanBenefits();
            planBenefits.setViewType(BenefitsAdapter.SAVE_VIEW_TYPE);
            String save = "Save Upto Rs " + String.valueOf(planModel.getPlanSaveUpto());
            planBenefits.setBenefitTitle(save);
            List<PlanBenefits> planBenefitsList = planModel.getPlanBenefitsList();
            planBenefitsList.add(planBenefits);
            if (benefitsAdapter != null) {
                benefitsAdapter.updateList(planBenefitsList);

            } else {
                benefitsAdapter = new BenefitsAdapter(context, planBenefitsList);
                recyclerview.setAdapter(benefitsAdapter);
            }
        }
    }
}
