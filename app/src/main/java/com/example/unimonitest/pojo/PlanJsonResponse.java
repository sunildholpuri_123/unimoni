package com.example.unimonitest.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlanJsonResponse {

    private int responseStatus;
    @SerializedName("plans")
    private List<PlanModel> planModelList;

    public int getResponseStatus() {
        return responseStatus;
    }

    public List<PlanModel> getPlanModelList() {
        return planModelList;
    }
}
