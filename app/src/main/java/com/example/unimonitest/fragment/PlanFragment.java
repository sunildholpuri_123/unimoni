package com.example.unimonitest.fragment;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.unimonitest.R;
import com.example.unimonitest.adapter.PlanAdapter;
import com.example.unimonitest.network.ApiClient;
import com.example.unimonitest.network.ApiInterface;
import com.example.unimonitest.pojo.PlanModel;
import com.example.unimonitest.utils.StateData;
import com.example.unimonitest.viewModel.PlanViewModel;

import java.util.List;


public class PlanFragment extends Fragment {

    PlanViewModel planViewModel;
    private Context context;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private PlanAdapter planAdapter;
    private int memberCount;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.member_plan_fragment, container, false);
        initializeWidgets(view);


        planViewModel = new PlanViewModel(ApiClient.getClient(getActivity()).create(ApiInterface.class));
        if (getArguments() != null) {
            memberCount = getArguments().getInt(context.getString(R.string.member_count));
            planViewModel.getJsonResponseLiveData().observe(this, Observable());
            if (planViewModel.getJsonResponseLiveData().getValue() != null) {
                setData(planViewModel.getJsonResponseLiveData().getValue().getData(), memberCount);
            } else {
                getMembershipPlan();
            }
        }
        return view;
    }

    private void getMembershipPlan() {

        planViewModel.getMemberPlan(memberCount);

    }

    private Observer<StateData<List<PlanModel>>> Observable() {
        return new Observer<StateData<List<PlanModel>>>() {
            @Override
            public void onChanged(@Nullable StateData<List<PlanModel>> listStateData) {
                assert listStateData != null;
                if (listStateData.getStatus() == StateData.DataStatus.SUCCESS) {
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    setData(listStateData.getData(), memberCount);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);

                }
            }
        };
    }

    private void setData(List<PlanModel> data, int memberCount) {
        if (planAdapter != null) {
            planAdapter.updateList(data, memberCount);
        } else {
            planAdapter = new PlanAdapter(context, data, memberCount);
            recyclerView.setAdapter(planAdapter);
        }
    }

    private void initializeWidgets(View view) {
        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        progressBar = view.findViewById(R.id.progressBar);

    }

}
