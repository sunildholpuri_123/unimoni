package com.example.unimonitest;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.unimonitest.adapter.PagerAdapter;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TabLayout mTabs = findViewById(R.id.tbPlan);
        ViewPager viewPager = findViewById(R.id.vwPager);
        String[] titleList = new String[]{getString(R.string.two_members), getString(R.string.four_members)};
        viewPager.setAdapter(new PagerAdapter(this, getSupportFragmentManager(), titleList));
        mTabs.setupWithViewPager(viewPager);
        setToolbar();
    }

    protected void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.member_selection));
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


}
