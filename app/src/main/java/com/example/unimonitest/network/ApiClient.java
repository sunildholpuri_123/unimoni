package com.example.unimonitest.network;

import android.content.Context;

import com.example.unimonitest.smartcall.BasicCaching;
import com.example.unimonitest.smartcall.SmartCallFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    public static final String BASE_URL = "http://demo4929648.mockable.io/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient(Context context) {
        if (retrofit == null) {
            SmartCallFactory smartFactory = new SmartCallFactory(BasicCaching.fromCtx(context));

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(smartFactory)
                    .build();
        }
        return retrofit;
    }
}
