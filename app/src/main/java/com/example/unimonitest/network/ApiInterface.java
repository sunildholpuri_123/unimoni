package com.example.unimonitest.network;


import com.example.unimonitest.pojo.PlanJsonResponse;
import com.example.unimonitest.smartcall.SmartCall;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("IHOPolicyDetails")
    SmartCall<PlanJsonResponse> getMemberPlan(
            @Query("member_count") int id
    );

}
