package com.example.unimonitest.viewModel;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.example.unimonitest.network.ApiInterface;
import com.example.unimonitest.pojo.PlanJsonResponse;
import com.example.unimonitest.pojo.PlanModel;
import com.example.unimonitest.smartcall.SmartCall;
import com.example.unimonitest.utils.StateLiveData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlanViewModel extends ViewModel {

    private ApiInterface apiInterface;

    private StateLiveData<List<PlanModel>> jsonResponseLiveData = new StateLiveData<>();

    public PlanViewModel(ApiInterface apiInterface) {
        this.apiInterface = apiInterface;
    }

    public StateLiveData<List<PlanModel>> getJsonResponseLiveData() {
        return jsonResponseLiveData;
    }

    public void getMemberPlan(int count) {
        SmartCall<PlanJsonResponse> call = apiInterface.getMemberPlan(count);

        call.enqueue(new Callback<PlanJsonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PlanJsonResponse> call, @NonNull Response<PlanJsonResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<PlanModel> resultList = response.body().getPlanModelList();
                    /*Setting success to live data*/
                    jsonResponseLiveData.postSuccess(resultList);
                } else {
                    jsonResponseLiveData.postComplete();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PlanJsonResponse> call, @NonNull Throwable t) {
                /*Setting Error to live data*/
                //jsonResponseLiveData.postError(t);
            }
        });
    }
}
